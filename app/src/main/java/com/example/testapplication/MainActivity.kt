package com.example.testapplication

import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.drawable.Drawable
import android.widget.*
import android.os.Bundle
import android.os.Environment
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.activity_main.*
import java.util.*
import androidx.appcompat.app.AppCompatActivity as AppCompatActivity1
import com.bumptech.glide.annotation.GlideModule
import android.os.Environment.DIRECTORY_PICTURES
import android.os.Environment.getExternalStoragePublicDirectory
import com.bumptech.glide.request.RequestOptions
import java.io.File


@GlideModule
class MainActivity : AppCompatActivity1() {

    var list = arrayOf("Женский", "Мужской")

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val aa = ArrayAdapter(this, android.R.layout.simple_spinner_item, list)
        // Set layout to use when the list of choices appear
        aa.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        // Set Adapter to Spinner
        spinner!!.setAdapter(aa)

        val c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

        pickDateBtn.setOnClickListener {

            val dpd = DatePickerDialog(this, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                // Display Selected date in TextView
                dateTv.setText("" + dayOfMonth + " " + month + ", " + year)
            }, year, month, day)
            dpd.show()
        }

        buttonSubmit.setOnClickListener {
            if (i_agree.isChecked) {
                i_agree.error = null
            } else {
                i_agree.error = "Вы не согласились с условиями"
            }

            if (login.getText().toString().equals("")) {
                login.error = "Это обязательное поле"
            } else {
                login.error = null
            }

            if (password.getText().toString().equals("")) {
                password.error = "Это обязательное поле"
            } else {
                password.error = null
            }

            if (passwordRepeat.getText().toString().equals("")) {
                passwordRepeat.error = "Это обязательное поле"
            } else {
                passwordRepeat.error = null
            }

            if (dateTv.getText().toString().equals("")) {
                dateTv.error = "Это обязательное поле"
            } else {
                dateTv.error = null
            }

            if (passwordRepeat.text.toString() == password.text.toString()) {
                passwordRepeat.error = null
            } else {
                passwordRepeat.error = "Пароли не совпадают"
            }

            if (login.error == null
                && dateTv.error == null
                && password.error == null
                && passwordRepeat.error == null
                && i_agree.error == null
            ) {

                val randomIntent = Intent(this, SecondActivity::class.java)
                randomIntent.putExtra("username", login.getText().toString())
                startActivity(randomIntent)
            }
        }

        Glide.with(this)
            .load( R.drawable.image)
            .apply(RequestOptions.circleCropTransform())
            .into(imageView2)
    }
}
